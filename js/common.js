﻿new function () {
    var _self = this;
    _self.width = 640;//设置默认最大宽度
    _self.fontSize = 100;//默认字体大小
    _self.widthProportion = function () { var p = (document.body && document.body.clientWidth || document.getElementsByTagName("html")[0].offsetWidth) / _self.width; return p > 1 ? 1 : p < 0.5 ? 0.5 : p; };
    _self.changePage = function () {
        document.getElementsByTagName("html")[0].setAttribute("style", "font-size:" + (document.documentElement.clientWidth / 10) + "px !important");
    }
    _self.changePage();
    window.addEventListener('resize', function () { _self.changePage(); }, false);
};

//手机
function isPoneAvailable(phone) {
    var myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
    if (!myreg.test(phone)) {
        return false;
    } else {
        return true;
    }
}
//正数
function isPositive(num) {
    var reg = /^\d+(?=\.{0,1}\d+$|$)/
    if (reg.test(num)) return true;
    return false;
}
//正整数
function isNum(num) {
    var reg = /^[1-9]\d*$/;
    if (reg.test(num)) return true;
    return false;
}

//身份证
function isCardNo(num) {
    var reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    if (reg.test(num)) return true;
    return false;
}

//url参数
function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]);
    return null;
}
//根据身份证计算年龄
function getAge(identityCard) {
    var len = (identityCard + "").length;
    if (len == 0) {
        return 0;
    } else {
        if ((len != 15) && (len != 18))//身份证号码只能为15位或18位其它不合法
        {
            return 0;
        }
    }
    var strBirthday = "";
    if (len == 18)//处理18位的身份证号码从号码中得到生日和性别代码
    {
        strBirthday = identityCard.substr(6, 4) + "/" + identityCard.substr(10, 2) + "/" + identityCard.substr(12, 2);
    }
    if (len == 15) {
        strBirthday = "19" + identityCard.substr(6, 2) + "/" + identityCard.substr(8, 2) + "/" + identityCard.substr(10, 2);
    }
    //时间字符串里，必须是“/”
    var birthDate = new Date(strBirthday);
    var nowDateTime = new Date();
    var age = nowDateTime.getFullYear() - birthDate.getFullYear();
    //再考虑月、天的因素;.getMonth()获取的是从0开始的，这里进行比较，不需要加1
    if (nowDateTime.getMonth() < birthDate.getMonth() || (nowDateTime.getMonth() == birthDate.getMonth() && nowDateTime.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

function ajaxPost(post_data, url, call_back) {
    var data = JSON.stringify(post_data);
    $.showLoading();
    $.ajax({
        type: "POST",
        url: url,
        data: data,
        dataType: "json",
        contentType: "application/json",
        success: function (res) {
            if (res.returnCode == "000") {
                if (res.info.status) {
                    if (res.info.status == 'SUCCESS') {
                        call_back(res.info);
                    } else {
                        $.toptip(res.info.errorMsg, 'error');
                    }
                } else {
                    call_back(res.info);
                }
            } else {
                $.toptip(res.returnDesc, 'error');
            }
        },
        error: function () {
            $.toptip('通信异常', 'error');
        },
        complete: function () {
            $.hideLoading();
        }

    })
}