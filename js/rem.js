// uglifyjs rem.js -m -o rem.min.js
! function (window) {
  var root = document.documentElement,
    eventType = 'resize', // 'orientationchange',
    fontSize,
    rootWidth,
    detectedWidth

  if (!(('on' + eventType) in window)) eventType = 'resize'

  function setRem() {
    rootWidth = root.clientWidth
    // var initSize =  rootWidth / 750 * 50
    fontSize = rootWidth / 750 * 200
    fontSize = fontSize > 120 ? 120 : fontSize < 80 ? 80 : fontSize
      console.log(fontSize)
    if (detectedWidth) fontSize = fontSize * rootWidth / detectedWidth
    root.style.fontSize = fontSize + 'px'
       // 部分引用jS会覆盖上面这层 故加入以下容错
    document.getElementsByTagName("html")[0].setAttribute("style", "font-size:" + fontSize + "px !important");
  }

  window.addEventListener(eventType, setRem, false)
  setRem()

  setTimeout(function fixRem() {
    if (document.body) {
      var div = document.createElement('div')
      div.style.width = (rootWidth / fontSize) + 'rem'
      document.body.appendChild(div)
      var divWidth = div.clientWidth
      if (parseInt(divWidth) > rootWidth) {
        detectedWidth = divWidth
        setRem()
      }
      document.body.removeChild(div)
      div = null
    } else {
      setTimeout(fixRem, 50)
    }
  }, 50)

}.call(this, this)
